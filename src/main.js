import Vue from 'vue';
import api from '@api';
import store from '@store';
import router from '@/router';
import globalMixin from '@/mixins/global';
import events from '@utils/events';
import vuetify from '@/plugins/vuetify';
import App from './App.vue';

import 'vuetify/dist/vuetify.min.css';

Vue.prototype.$api = api;
Vue.prototype.$events = events;

Vue.mixin(globalMixin);

window.app = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');
