import api from '@api';

const getters = {
  getAspects: state => state.list,
};

const actions = {
  setAspectsByShopRequest({ commit }, data) {
    return api.aspects.getAspectsByShop(data).then((res) => {
      commit('setAspects', res);
    });
  },
};

const mutations = {
  setAspects(state, data) {
    state.list = data;
  },
};

const state = {
  list: {},
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
