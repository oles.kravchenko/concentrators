import api from '@api';

const getters = {
  getShops: state => state.list,
};

const actions = {
  fetchShopsRequest({ commit }, data) {
    return api.shops.getShops(data).then((res) => {
      commit('setShops', res);
    });
  },
};

const mutations = {
  setShops(state, data) {
    state.list = data;
  },
};

const state = {
  list: [],
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
