import http from '@utils/http';

export default {
  getAspectsByShop(id) {
    return new Promise((resolve, reject) => {
      http.get(`/api/con/aspects/shop/${id}`).then(
        ({ data }) => {
          resolve(data);
        },
      ).catch((error) => {
        reject(error);
      });
    });
  },
  getAspectsByBrand(id) {
    return new Promise((resolve, reject) => {
      http.get(`/api/con/aspects/brand/${id}`).then(
        ({ data }) => {
          resolve(data);
        },
      ).catch((error) => {
        reject(error);
      });
    });
  },
};
