import http from '@utils/http';

export default {
  getShops(id) {
    return new Promise((resolve, reject) => {
      http.get(`/api/con/shops/${id}`).then(
        ({ data }) => {
          resolve(data);
        },
      ).catch((error) => {
        reject(error);
      });
    });
  },
  getShop(id) {
    return new Promise((resolve, reject) => {
      http.get(`/api/con/aspects/shop/${id}`).then(
        ({ data }) => {
          resolve(data);
        },
      ).catch((error) => {
        reject(error);
      });
    });
  },
};
