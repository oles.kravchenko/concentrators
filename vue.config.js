const path = require('path');

function resolve(dir) {
  return path.join(__dirname, dir);
}
module.exports = {
  transpileDependencies: [
    'vuetify',
  ],
  runtimeCompiler: true,
  productionSourceMap: process.env.NODE_ENV !== 'production',
  chainWebpack: (config) => {
    config.resolve.alias.set('@', resolve('src'));
    config.resolve.alias.set('@api', resolve('api'));
    config.resolve.alias.set('@views', resolve('src/views'));
    config.resolve.alias.set('@store', resolve('src/store'));
    config.resolve.alias.set('@router', resolve('src/router'));
    config.resolve.alias.set('@components', resolve('src/components'));
    config.resolve.alias.set('@utils', resolve('src/utils'));
    config.resolve.alias.set('@helpers', resolve('src/helpers'));
    config.resolve.alias.set('@config', resolve('src/config'));
    config.resolve.alias.set('@constants', resolve('src/constants'));
    config.resolve.alias.set('@mixins', resolve('src/mixins'));
  },
  css: {
    loaderOptions: {
      scss: {
        prependData:
            '@import "~@/styles/_variables.scss";',
      },
    },
  },

  outputDir: process.env.OUTPUT_DIR,
  indexPath: 'index.html',
};
